#!/usr/bin/env bash
# Bash v4.1+ highly recommended (minimum tested version is 4.3.30(1)-release)
# Copyright © 2015~2016 GhettoGirl ─ github.com/GhettoGirl
##########################################################################################################################
## Wine Launcher Template                                                                                               ##
##########################################################################################################################
#                                                                                                                        #
# Useful helper script to easily handle Windows Applications with wine.                                                  #
# Don't stuff .desktop files full with unnecessary environment variables or even hackish one-liner scripts. :/           #
# Use this easy to configure script instead for your .desktop files ;)                                                   #
# It makes your life easier when working with multiple WINEPREFIXes - I promise (:                                       #
#                                                                                                                        #
#  === Script Configuration ===                                                                                          #
#                                                                                                                        #
#   $WINEROOTPATH        Wine Installation to use (by default the script uses the system default)                        #
#                        Example: /path/to/wine/version                                                                  #
#   $WINETRICKSCMD       WineTricks command to use (by default the script uses the system default)                       #
#                                                                                                                        #
#   $WINEPREFIX          /path/to/wineprefix                                                                             #
#   $WINEARCH            win32 for 32-bit, win64 for 64-bit                                                              #
#                                                                                                                        #
#   $WINEMAINEXE         DOS-PATH to executable the launcher should use (eg: C:/Program Files/Some Program/program.exe)  #
#                                                                                                                        #
#  === Command Line Arguments ===                                                                                        #
#                                                                                                                        #
#   [...]                  Run main executable with arguments (DON'T INCLUDE PROGRAM NAME)                               #
#   winecfg [...]          Launch the Wine Configuration Dialog                                                          #
#   winetricks [...]       Run winetricks                                                                                #
#   wineserver [...]       Control the wineserver                                                                        #
#   winecontrol [...]      Run the Wine Control Panel                                                                    #
#                                                                                                                        #
##########################################################################################################################

### User Configuration ###################################################################################################

# Wine Installation
WINEROOTPATH="$(dirname $(which wine))"
WINETRICKSCMD="$(which winetricks)"

# Add additional library paths if needed, required for "special" wine builds (like PlayOnLinux/PlayOnMac)
#export WINETRICKS_LD_LIBRARY_PATH="$LD_LIBRARY_PATH"
#export LD_LIBRARY_PATH="$WINEROOTPATH/../lib:$WINEROOTPATH/../lib64:$LD_LIBRARY_PATH"

# Wine Environment
export WINEPREFIX=""
export WINEARCH=""

# Program Executable
WINEMAINEXE="C:/"

# OPTIMAL: for hybrid graphics users, insert 'optirun', 'primusrun' or whatever you're using here
#          for graphic intensive programs like games, keep empty if not necessary
GLX_LAUNCH=""

### Error Checking #######################################################################################################

### Helper function to print errors and exit
function winelauncher_error() {
    echo -e "Wine Launcher: $1"
    exit $2
}

### Check if WINE environment got configured
if [[ -z "$WINEPREFIX" || -z "$WINEARCH" ]]; then
    winelauncher_error "Configure WINE environment first!" 1
fi

if [[ "$WINEARCH" != "win32" && "$WINEARCH" != "win64" ]]; then
    winelauncher_error "WINEARCH is invalid! only 'win32' and 'win64' allowed. current = $WINEARCH" 1
fi

### Validate the WINEROOTPATH for correctness ─ only check for the bare minimum
if ! [[ -x "$WINEROOTPATH/wine" && \
        -x "$WINEROOTPATH/winecfg" && \
        -x "$WINEROOTPATH/wineserver" && \
        -x "$WINEROOTPATH/wineboot" && \
        -x "$WINEROOTPATH/wine-preloader" ]]; then
    winelauncher_error "Wine Installation not valid! Please check -> $WINEROOTPATH" 1
fi

if ! [[ -x "$WINETRICKSCMD" ]]; then
    winelauncher_error "WineTricks not found or not executable -> $WINETRICKSCMD" 1
fi

### Check the GLX_LAUNCH command
# If not empty than we want to use hybrid graphics, so check if command is valid
if [[ ! -z "$GLX_LAUNCH" ]]; then
    # NOTE: which returns 1 if command contains spaces, so we need to check its output rather than its exit code
    if [[ -z $(which $GLX_LAUNCH) ]]; then
        winelauncher_error "GLX_LAUNCH command not valid! Validate file path and syntax." 1
    fi
fi

### Command Line Parsing #################################################################################################

# Wine Configuration
if   [[ "$1" == "winecfg" ]]; then
        shift
        "$WINEROOTPATH/winecfg" "$@"
        exit $?
elif [[ "$1" == "winetricks" ]]; then
        shift
        export LD_LIBRARY_PATH="$WINETRICKS_LD_LIBRARY_PATH"
        "$WINETRICKSCMD" "$@"
        exit $?
elif [[ "$1" == "wineserver" ]]; then
        shift
        "$WINEROOTPATH/wineserver" "$@"
        exit $?
elif [[ "$1" == "winecontrol" ]]; then
        shift
        "$WINEROOTPATH/wine" control "$@"
        exit $?

# Main Executable
else
    $GLX_LAUNCH "$WINEROOTPATH/wine" "$WINEMAINEXE" "$@"
    exit $?
fi

##########################################################################################################################
