# Miscellaneous Utilities

Minor useful utilities to make your life easier.


Tool           | Short Description  | Language
-------------- | ------------------ | --------
Wine Launcher  | Helper script to easily handle Windows Applications with wine. | Bash</br>4.1+
Steam Launcher | Starts *STEAM* with *GAME* and closes everything after the *GAME* exits. No need to close __STEAM__ by hand anymore (:  This is very helpful if using wine with several *WINEPREFIX*es for example. Go ahead and create your *.desktop* launchers for every game. | Bash</br>4.1+
vm-usb-flashmode | Convenient helper script to prepare the Linux kernel for virtual machine USB access. To flash ROM or Firmware data onto devices over USB within a virtual machine you first need to get rid of the USB control in the Linux kernel or USB devices keeps resetting and reconnecting to the host OS (Linux) which prevents actual ROM or Firmware flashing. </br> A list of tested devices is embedded in the script :) | Bash</br>2.0+


For more detailed descriptions, usage and the supported platforms, refer to the embeded documentation in the scripts itself.

### Licensing and Warrenty

All scripts comes without any kind of warrenty or a license, use it and share it as you wish. Of course I would appreciate it if you don't remove the copyright line at the top of every script, and mark the script as modified if you do any changes to it.

Suggestions and improvements are welcome (:
